# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO
**NAME**: ALEXEY KUZYUSHIN

**E-MAIL**: alexeykuzyushin@yandex.ru

# SOFTWARE

- JDK 1.8
- Ubuntu 20.10

# HARDWARE

- CPU: Intel i5-3337U or highter
- RAM: 8 Gb DD3-1600MHz or highter

# PROGRAM RUN

``` bash
java -jar ./task-manager.jar
```
